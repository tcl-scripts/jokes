#
# Jokes version 2.2.3
#
# Have jokes on your channel
#
# Author: CrazyCat <crazycat@c-p-f.org>
# http://www.eggdrop.fr
# irc.zeolia.net #eggdrop
#
#
### USAGE ###
# To authorize the use of jokes in a channel, add it the +jokes flag
# example: .chanset #dummy +jokes
# To authorize the use of adult jokes in a channel, add it the +Ajokes flag
#	(it needs the +jokes to)
# example: .chanset #dummy +Ajokes
# To allow someone to use the adult jokes, give him a global A flag
# example: .chattr n00b +A
#
# -- Commands --
# /msg <botnick> jokes
#	Give a list of available jokes
# /msg <botnick> jokes reload
#	Reload the database
# /msg <botnick> jokes save
#	Saves the database and reload it
# /msg <botnick> jokes add <[!]keyword>
#	Adds "keyword" as a new joke keyword. If the keywords is preceded of !,
#	the joke is an adult one.
# /msg <botnick> jokes novict <[!]keyword> <The sentence to use>
#	Associates <The sentence to use> with the <[!]keyword>
#	It's used when no "victim" is designated
# /msg <botnick> jokes vict <[!]keyword> <The sentence to use>
#	Associates <The sentence to use> with the <[!]keyword>
#	It's used when a "victim" is designated
#
### DATABASE ###
# The database is using a really simple format:
# keyword=sentence without victim=Sentence with victim
# * keyword: if preceded by a !, it means that it's an adult joke
# * sentence: it is a simple sentence with 3 possible arguments:
#	 %r is a random victim
#	 %n is the nick of the person who launch the joke
#	 %v is the designated victim (in fact, everything after the joke)
# ** A sentence wich begins with ME will be an action
#
### CHANGELOG ###
#   2.2.3 - Corrected errors with nicks containing special chars
#         - Corrected trouble with colors unevaluated (MenzAgitat tip)
#   2.2.2 - Corrections of small troubles (backslashes)
#	2.2.1 - Using namespaces, correcting bugs (�MenzAgitat & CrazyCat)
#	2.2 - Settings are now using flags
#	2.1 - "Special jokes" added
#	2.0 - Using a file as database
#	1.4 - msg command (help)
#	1.2 - Adding protected people and authorized channels
#	1.1 - Replacing multiple proc by only one
#	1.0 - Initial script
#
# TODO:
# Allowing the modification of the DB
# with msg channel
#
### THANKS ###
# Special thanks to MenzAgitat for helping me
# adding the namespace and correcting the special
# characters bug.


namespace eval jokes {
	# The path and db name (relative to eggdrop position)
	variable jname "databases/jokes.db"

	# List of protected nicks, space separated
	# Protected peoples will never be a random victim
	variable protected "\[Gamer\] Denora"

	setudef flag jokes
	setudef flag Ajokes
	variable jokes_ver "2.2.3"
	variable jokes_author "CrazyCat <crazycat@c-p-f.org>"
}

proc jokes::load {} {
	variable joke_key_gen
	array set joke_key_gen {}
	variable joke_key
	array set joke_key {}
	set jp [open $jokes::jname "r"]
	set jdata [read -nonewline $jp]
	close $jp
	variable j_list ""
	variable j_slist ""
	foreach templine [split $jdata "\n"] {
		set joke_line [split $templine "="]
		set jkey [string tolower [lindex $joke_line 0]]
		if {[string index $jkey 0] == "!"} {
			set jkey [string range $jkey 1 end]
			lappend j_slist $jkey
		} else {
			lappend j_list $jkey
		}
		set joke_key_gen($jkey) [lindex $joke_line 1]
		set joke_key($jkey) [lindex $joke_line 2]
		bind pubm - "*:$jkey*" jokes::display
	}
}
jokes::load

proc jokes::display {nick uhost handle chan text} {
	if {[lsearch -exact [channel info $chan] +jokes] == -1} { return 0 }
	set args [split $text " "]
	set key [stripcodes abcgru [lindex $args 0]]
	set key [string range $key 1 end]
	if {![info exists jokes::joke_key_gen]} { return 0 }
	if {[lsearch $jokes::j_list $key]==-1 && [lsearch $jokes::j_slist $key]==-1} { return 0 }
	if {[lsearch $jokes::j_slist $key]>=0 && ([lsearch -exact [channel info $chan] +Ajokes]==-1 || ![matchattr $handle A $chan])} {
		putserv "PRIVMSG $nick :Acc�s interdit"
		return 0
	}
	set text [string trim [join [lrange $args 1 end]]]
	if {[string length $text] > 0} {
		regsub -all -- \\\\ $text \\\\\\\\ text
		set answer $jokes::joke_key($key)
		regsub -all %v $answer "$text" answer
	} else {
		set answer $jokes::joke_key_gen($key)
	}
	regsub -all %n $answer $nick answer
	set randvic [jokes::random_victim $nick $chan]
	if {$randvic == 0} {
		putserv "PRIVMSG $chan :Hey $nick, tu joues seul?"
		return 0
	}
	regsub -all %r $answer $randvic answer
	regsub -all {\W} $answer {\\&}
	if {[lindex $answer 0] == "ME"} {
		putserv "PRIVMSG $chan :\001ACTION [join [lrange $answer 1 end]]\001"
	} else {
		putserv "PRIVMSG $chan :[join $answer]"
	}
}

proc jokes::random_victim {nick chan} {
	set user_list [chanlist $chan]
	set exclist [split "$::botnick $nick $jokes::protected"]
	foreach exclude [split $exclist] {
		set wb [lsearch $user_list $exclude]
		set user_list [lreplace $user_list $wb $wb]
	}
	if {[llength $user_list]>0} {
		return [lindex $user_list [rand [llength $user_list]]]
	} else	{
		return 0
	}
}

bind pubm - "*:jokes" jokes::publist
proc jokes::publist {nick uhost handle chan text} {
	if {[lsearch -exact [channel info $chan] +jokes] == -1} { return 0 }
	set text [stripcodes abcgru $text]
	set thiskey [string range [lindex $text 0] 1 [string length [lindex $text 0]]]
	if { $thiskey != "jokes" } { return 0 }
	putserv "NOTICE $nick :La commande est /msg $::botnick jokes"
	joke:list $nick $uhost $handle ""
}

bind msg - "jokes" jokes::list
proc jokes::list {nick uhost handle args} {
	set arg [lindex [split $args] 0]
	if {$arg == "reload"} {
		jokes::load
		putserv "NOTICE $nick :-- \002Reload OK\002 --"
		return 1
	}
	puthelp "NOTICE $nick :-- \002Liste des blagues\002 --"
	set j_compt 0
	set cur_keys ""
	foreach j_elem $jokes::j_list {
		incr j_compt
		append cur_keys " $j_elem"
		if {$j_compt == 10} {
			puthelp "NOTICE $nick :$cur_keys"
			set cur_keys ""
			set j_compt 0
		}
	}
	if {$j_compt>0} {
		puthelp "NOTICE $nick :$cur_keys"
	}
	if {[matchattr $handle A]} {
		puthelp "NOTICE $nick :-- \002Liste des blagues adultes\002 --"
		set j_compt 0
		set cur_keys ""
		foreach j_elem $jokes::j_slist {
			incr j_compt
			append cur_keys " $j_elem"
			if {$j_compt == 10} {
				puthelp "NOTICE $nick :$cur_keys"
				set cur_keys ""
				set j_compt 0
			}
		}
		if {$j_compt>0} {
			puthelp "NOTICE $nick :$cur_keys"
		}
	}
	return 1
}

putlog "Jokes version $jokes::jokes_ver by $jokes::jokes_author loaded"
